#ifndef _ObsBox2Spectrum_FFT_H_
#define _ObsBox2Spectrum_FFT_H_

#include <complex>
#include <fftw3.h>
#include <immintrin.h>
#include <math.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <vector>
#define cores 4

using namespace std::complex_literals;

class ObsBox2SpectrumFFT {
public:
  ObsBox2SpectrumFFT(const std::size_t &samples) {
    _samples = samples;
    int status = fftw_init_threads();
    (void)status;
    fftw_plan_with_nthreads(cores);
    buffer = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * _samples);
    output = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * _samples);
    forward = fftw_plan_dft_1d(static_cast<int>(_samples), buffer, output,
                               FFTW_FORWARD, FFTW_MEASURE);

    for (double i = 0.0; i < revolution_frequency;
         i += revolution_frequency / static_cast<double>(_samples)) {
      freqs.push_back(i);
    }
    average_fft =
        std::vector<std::complex<double>>(_samples, std::complex(0.0, 0.0));
    average_fft_corrected =
        std::vector<std::complex<double>>(_samples, std::complex(0.0, 0.0));
  }
  ~ObsBox2SpectrumFFT() {
    fftw_free(output);
    fftw_free(buffer);
    fftw_destroy_plan(forward);
    fftw_cleanup_threads();
    fftw_cleanup();
  }

  void resetAverage() {
    average_fft =
        std::vector<std::complex<double>>(_samples, std::complex(0.0, 0.0));
    average_fft_corrected =
        std::vector<std::complex<double>>(_samples, std::complex(0.0, 0.0));
    counter = 0;
  }

  std::vector<double> getFreqs() { return freqs; }

  std::vector<double> getRealOutput() {
    std::complex<double> *output_complex =
        reinterpret_cast<std::complex<double> *>(output);
    std::vector<double> to_return;
    for (std::size_t i = 0; i < _samples; i++) {
      to_return.push_back((*(output_complex + i)).real());
    }
    return to_return;
  }
  std::vector<std::complex<double>> getOutput() {
    std::complex<double> *output_complex =
        reinterpret_cast<std::complex<double> *>(output);
    std::vector<std::complex<double>> to_return;
    for (std::size_t i = 0; i < _samples; i++) {
      to_return.push_back((*(output_complex + i)));
    }
    return to_return;
  }
  std::vector<double> getImgOutput() {
    std::complex<double> *output_complex =
        reinterpret_cast<std::complex<double> *>(output);
    std::vector<double> to_return;
    for (std::size_t i = 0; i < _samples; i++) {
      to_return.push_back((*(output_complex + i)).imag());
    }
    return to_return;
  }

  bool setData(const int16_t *data, const std::size_t &length) {
    if (length != _samples) {
      return false;
    }
    for (std::size_t i = 0; i < _samples; i++) {
      *(buffer + i)[0] = static_cast<double>(*(data + i));
      *(buffer + i)[1] = 0.0;
    }

    return true;
  }

  void calculate(const std::size_t &x0 = 0, const std::size_t &x = 0) {
    fftw_execute(forward);
    counter++;
    std::complex<double> *output_complex =
        reinterpret_cast<std::complex<double> *>(output);
    output_vector.clear();
    for (std::size_t i = 0; i < _samples; i++) {
      output_vector.push_back((*(output_complex + i)));
    }
    for (std::size_t i = 0; i < output_vector.size(); i++) {
      output_vector[i] =
          output_vector[i] / static_cast<double>(output_vector.size()) * 2.0;
    }
    double delay = static_cast<double>(x - x0) * sampling_period;
    for (std::size_t i = 0; i < _samples; i++) {
      average_fft[i] += output_vector[i];
      average_fft_corrected[i] +=
          output_vector[i] * std::exp(-1i * 2.0 * M_PI * freqs[i] * delay);
    }
  }

  std::vector<double> getAverage() {
    std::vector<double> to_return(_samples, 0);

    for (std::size_t i = 0; i < average_fft.size(); i++) {
      to_return[i] =
          std::log(std::abs((average_fft[i] / static_cast<double>(counter))));
    }
    return to_return;
  }
  std::vector<double> getAverageCorrected() {
    std::vector<double> to_return(_samples, 0);

    for (std::size_t i = 0; i < average_fft_corrected.size(); i++) {
      to_return[i] =
          std::abs((average_fft_corrected[i] / static_cast<double>(counter)));
    }
    return to_return;
  }

protected:
private:
  // double* buffer=NULL;
  fftw_complex *output = NULL;
  fftw_complex *buffer = NULL;
  fftw_plan forward = NULL;

  std::vector<std::complex<double>> output_vector;
  std::vector<std::complex<double>> average_fft;
  std::vector<std::complex<double>> average_fft_corrected;

  std::size_t _samples;
  std::size_t counter = 0;
  double revolution_frequency = 11245.5;
  double sampling_period = 25e-9;
  std::vector<double> freqs;
};

#endif
