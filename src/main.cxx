#include "FFT.h"
#include "HDFLib.h"
#include "matplotlibcpp.h"
#include <cassert>
namespace plt = matplotlibcpp;
using namespace HDFLib;
using namespace std::complex_literals;
int main(int args, char **argv) {

  assert(args == 2);
  std::string filename = std::string(argv[1]);
  HDFFile file = HDFFile(filename);
  file.open();
  std::size_t columns = file.getColumns();
  std::size_t rows = file.getRows();
  std::cout << "bunches: " << columns << " samples: " << rows << std::endl;

  ObsBox2SpectrumFFT *FFT = new ObsBox2SpectrumFFT(rows);

  std::vector<std::size_t> bunches;
  auto data = file.getData();
  for (std::size_t bunch = 0; bunch < columns; bunch++) {
    bool empty = true;
    for (std::size_t turn = 0; turn < rows; turn++) {
      if (*(data.get() + bunch + columns * turn) != 0) {
        empty = false;
      }
    }
    if (!empty) {
      bunches.push_back(bunch);
    }
  }
  int16_t *temp_buffer =
      reinterpret_cast<int16_t *>(malloc(rows * sizeof(int16_t)));

  for (auto bunch : bunches) {
    // fill buffer
    for (std::size_t row = 0; row < rows; row++) {
      *(temp_buffer + row) = *(data.get() + bunch + columns * row);
    }

    FFT->setData(temp_buffer, rows);
    FFT->calculate(bunches[0], bunch);
  }
  plt::plot(FFT->getFreqs(), FFT->getAverageCorrected());
  plt::show();
}
